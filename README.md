# DEMO Land Monitoring Caching

This repo is created by Python Language. The purpose is caching data on AWS S3 bucket sentinel2 and GCP cloud storage bucket gcp-public-data-landsat at local.

This repo is tested with CentOS 7, Ubuntu 18.06 environment.

### **AWS sentinel2 Include**
1. Specific AWS Prefix (download only prefix setting).
2. Create directory followed AWS structure.
3. Calculation files size.
4. Check files exits.
5. Download files from AWS S3.
6. Logging.

### **GCP landsat Include**
1. Specific year (download all Thailand tiles).
2. Create directory followed GCP structure.
3. Calculation files size.
4. Check files exits.
5. Check free disk space.
6. Download files from GCP cloud storage.
7. Logging.


## **AWS/GCP Requirements**

- _Install python 3.8 or create python virtual environment_

[`Python 3 Docs`](https://docs.python.org/3/library/venv.html)

- _Configure AWS credential on host_ 

[`AWS Configuration and credential file settings`](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

- _Configure GCP credential on host_

[`GCP Configuration and credential file settings`](https://cloud.google.com/docs/authentication/production)

- _Install packages from requirements file_

 `pip install -r requirements.txt`


## **Running**
### **AWS**
- Edit specific bucket(sentinel2) and prefix (aws_sentinel2_download.py)

        # CHANGE HERE
        BUCKET = 'sentinel-s2-l2a'      # default bucket
        PREFIX = 'tiles/47/P/QR/2020/9/13/0/R10m/'        # default prefix
        DIR = os.path.abspath('../')+'/sentinel-s2-l2a/'        # default store path

- Run

        python aws_sentinel2_download.py

- Monitoring

        tail -f aws_job_log_{{date}}.log

### **GCP**
- Edit specific year for download(bucket landsat) all Thailand tiles (lansat-gcp-main.py)
       
        def GetFile(wrs2thai):
            # CHANGE HERE
            year = "20200928"

- Run

        python lansat-gcp-main.py

- Monitoring

        Log files located in Logs directory.



