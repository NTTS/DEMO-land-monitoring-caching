# aws_sentinel2_download is python script for download files on AWS S3 to local
# Copyright (C) 2020 Surote Wongpaiboon.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import os

def list_all_dir(directory_name):
    """List all path name function"""
    current_dirpath = []
    for dirpath, dirs, files in os.walk(directory_name):
        current_dirpath.append(dirpath)
    return current_dirpath


def check_exist_file(directory_name):
    """List all file exist"""
    current_dirpath = []
    for dirpath, dirs, files in os.walk(directory_name):
        for file_name in files:
           current_dirpath.append(os.path.join(dirpath,file_name))
    return current_dirpath
