# aws_sentinel2_download is python script for download files on AWS S3 to local
# Copyright (C) 2020 Surote Wongpaiboon.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import boto3
from boto3.s3.transfer import TransferConfig
import os
import time
import datetime
import logging
import concurrent.futures
from sharefunction import list_all_dir as sh_list_all_dir
from sharefunction import check_exist_file as sh_check_exist_file
from hurry.filesize import size

logging.basicConfig(
    filename = 'aws_job_log_'+str(datetime.datetime.now()).split(' ')[0]+'.log',
    level = logging.INFO
    )
client = boto3.client('s3')

BUCKET = 'sentinel-s2-l2a'      # default bucket
PREFIX = 'tiles/47/P/QR/2020/9/13/0/R10m/'        # default prefix

DIR = os.path.abspath('../')+'/sentinel-s2-l2a/'        # default store path


def download_file(key_tile):
    """
    Download file from AWS.

    :param key_tile: the url path for download.

    :Example:
        download_file('tiles/47/N/QG/2020/9/10/0/R10m/AOT.jp2')
    :Returns:
        -

    """
    # Disable thread use/transfer concurrency
    config = TransferConfig(use_threads=True)
    logging.info('Downloading: ' + key_tile)
    client.download_file(BUCKET, key_tile, DIR + key_tile, ExtraArgs={'RequestPayer': 'requester'}, Config=config)
    logging.info('\n####\nDownloaded: ' + key_tile + '\n####')
    return DIR+key_tile+" DONE!"


def create_directory(dirpath):
    """
    Create directory recursively to the leaf file.

    :param dirpath: the directory path for create

    :Example:
        create_directory('tiles/47/P/QR/2020/9/13/0/R10m')
    :Returns:
        -

    """
    current_dirpath = sh_list_all_dir(DIR)
    leaf_directory = dirpath        # dirpath example : tiles/47/P/QR/2020/9/13/0/R10m
    
    if DIR+leaf_directory not in current_dirpath:       # check existing directory
        logging.info('Create Directory: ' + DIR+leaf_directory)
        try:
            os.makedirs(DIR+leaf_directory)
        except:
            logging.info(DIR+leaf_directory + ' Directory Exist!! with exception')
    else:
        logging.info(DIR+leaf_directory + ' Directory Exist!!')


def list_blob(bucket_name,prefix):
    """
    List all blob on AWS sentinel2 bucket

    :param bucket_name: bucket name on AWS 
    :param prefix: bucket followed by prefix to locate path ({bucket}/{prefix} => "sentinel-s2-l2a/tiles/47/P/QR/2020/9/")

    :Example:
        list_blob('sentinel-s2-l2a','tiles/47/P/QR/2020/9/')
    :Returns:
        2 lists of keys, leaf_directory
        [tiles/47/N/QG/2020/9/10/0/R10m/AOT.jp2',...],['tiles/47/N/QG/2020/9/10/0/R10m/',...]

    """
    paginator = client.get_paginator('list_objects')

    operation_parameters = {'Bucket': bucket_name,
                            'Prefix': prefix,
                            'RequestPayer': 'requester'}
    page_iterator = paginator.paginate(**operation_parameters)
    all_key = []
    all_directory = []
    all_size = 0
    key_size_tuple = []

    all_exist_key = sh_check_exist_file(DIR)

    for page in page_iterator:
        for i in page['Contents']:
            if DIR + i['Key'] not in all_exist_key:
                csize = str(size(i['Size']))
                all_size += i['Size']
                msg = i['Key'] +' SIZE: ' + csize
                logging.info(msg+' New file!')
                all_key.append(i['Key'])
                key_size_tuple.append((i['Key'],i['Size']))
                if ('/').join(i['Key'].split('/')[:-1]) not in all_directory:       # check duplicate directory path in list.
                    all_directory.append(('/').join(i['Key'].split('/')[:-1]))      # get directory from splitting filename.
            else:
                logging.info(i['Key']+' File exist!')
    logging.info('Total files size of prefix '+prefix+' is: ' + size(all_size))

    return all_key,all_directory,key_size_tuple,all_size


if __name__ == '__main__':
    all_key,all_directory,key_size_tuple,all_size = list_blob(BUCKET,PREFIX)        # call list_blob for list all key,directories for prepare download
    
    for dirpath in all_directory:       # Loop create directory
        logging.info('Sending to function : create_directory with '+dirpath)
        create_directory(dirpath)

    
    # download all files with thread
    logging.info("Running threaded:")
    threaded_start = time.time()
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        threads = []
        for key in all_key:        # download only new file
            # print(key)
            threads.append(executor.submit(download_file, key))     # download_file(key) with thread
        for thread in concurrent.futures.as_completed(threads):
            logging.info(thread.result())
    logging.info("Threaded time:"+ str(time.time() - threaded_start))
    
